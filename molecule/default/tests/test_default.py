import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_conserver_paths(host):
    assert host.file("/var/log/conserver").exists
    assert host.file("/var/log/conserver").is_directory
    assert host.file("/etc/conserver").exists
    assert host.file("/etc/conserver").is_directory
    assert host.file("/etc/conserver/procs.cf").exists
    assert host.file("/etc/conserver/procs.cf").is_file

    conserver_config_path = "/etc"

    assert host.file(os.path.join(conserver_config_path, "conserver.cf")).exists
    assert host.file(os.path.join(conserver_config_path, "conserver.cf")).is_file
    assert host.file(os.path.join(conserver_config_path, "conserver.passwd")).exists
    assert host.file(os.path.join(conserver_config_path, "conserver.passwd")).is_file


def test_conserver_procs_cf(host):
    if host.ansible.get_variables()["inventory_hostname"].startswith("ics-ans-role-conserver-server-consoles"):
        assert host.file("/etc/conserver/procs.cf").contains("console ioc-test-console")
        assert host.file("/etc/conserver/procs.cf").contains("console ioc-test_console-defaults")
        assert host.file("/etc/conserver/procs.cf").contains("uds /run/ioc@ioc-test-console/control")
        assert host.file("/etc/conserver/procs.cf").contains("uds /run/ioc@ioc-test_console-defaults/control")
        assert host.file("/etc/conserver/procs.cf").contains("master localhost")
        assert host.file("/etc/conserver/procs.cf").contains("master ioc_server")
        assert host.file("/etc/conserver/procs.cf").contains("type uds")
    else:
        assert not host.file("/etc/conserver/procs.cf").contains("console")


def test_conserver_server_enabled_and_running(host):
    service_name = "conserver"
    service = host.service(service_name)
    assert service.is_enabled
    assert service.is_running


def test_conserver_cf(host):
    if host.ansible.get_variables()["inventory_hostname"].startswith("ics-ans-role-conserver-server-consoles"):
        assert host.file("/etc/conserver.cf").contains("trusted trusted-host-1;")
        assert host.file("/etc/conserver.cf").contains("trusted trusted-host-2;")
        assert host.file("/etc/conserver.cf").contains("allowed allowed-host;")
        assert host.file("/etc/conserver.cf").contains("rejected rejected-host;")
