# ics-ans-role-conserver-server

Ansible role to install [conserver server](https://www.conserver.com).
Currently assumes that conserver client connects via UDS.
Changes will be required to support telnet ports.

## Role Variables


```yaml
conserver_server_host: Conserver server hostname
conserver_server_consoles:
  - master: IOC host - must be 'localhost' if type = 'uds' [default: localhost]
    name: IOC name for conserver usage (required)
    type: console type (typically 'uds') [default: uds]
    uds: path to UNIX domain socket [default: /run/ioc@<name>/control]
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-conserver-server
```

## License

BSD 2-clause
